package mstn.clearg.appengine;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import mstn.clearg.language.cgProcedure;
import mstn.clearg.appengine.UnauthorizedException;

import org.apache.velocity.exception.ResourceNotFoundException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class CGAppEngine {

	private String token;
	private String uri;
	private static Client clientRequest;
	private String schema;
	private static InitialContext initialContext;
	static{
		clientRequest = Client.create();
		try {
			initialContext = new InitialContext();
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	public CGAppEngine(String schema, String token) {
		this.token = token;
		this.uri = getGlobalVar("cgApp-Url");
		this.schema = schema;
	}
	
	public JSONObject getResource(String uri,
			MultivaluedMap<String, String> queryParams) throws Exception{
		return new JSONObject(getResourceData(uri, queryParams));
	}

	public String postResourceData(String uri,
			MultivaluedMap<String, String> queryParams) throws Exception {
		try {
			WebResource resource = clientRequest.resource(uri);

			ClientResponse clientResponse = resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, queryParams);
			
			//ClientResponse clientResponse = resource.queryParams(queryParams).get(ClientResponse.class);

			String content = clientResponse.getEntity(String.class);

			return content;
		} catch (Exception e) {
			throw new Exception(concat("Error: ", e.getMessage(), " Cause: ", e
					.getCause().toString(), " uri: ", uri));
		}
	}
	
	public String getResourceData(String uri, MultivaluedMap<String, String> queryParams) throws Exception {
		try {
			WebResource resource = clientRequest.resource(uri);

			//ClientResponse clientResponse = resource.queryParams(queryParams).get(ClientResponse.class);
			ClientResponse clientResponse = resource.post(ClientResponse.class,queryParams);

			String content = clientResponse.getEntity(String.class);

			return content;
		} catch (Exception e) {
			throw new Exception(concat("Error: ", e.getMessage(), " Cause: ", e
					.getCause().toString(), " uri: ", uri));
		}
	}

	public void setToken(String token) {
		this.token = token;
	}
	public String login(String username, String password, String schema) throws Exception{
		return login(username, password, schema,"");
	}
	
	public String login(String username, String password, String schema, String token) throws Exception {
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add("username", username);
		queryParams.add("password", password);
		queryParams.add("schema", schema);
		queryParams.add("token", token);
		this.token = getResource(concat(this.uri, "login"), queryParams).getString("content");
		return this.token;
	}
	
	public String logout(String token)
			throws Exception {
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add("token", token);
		this.token = getResource(concat(this.uri, "logout"), queryParams)
				.getString("content");
		return this.token;
	}

	public boolean isValid(String token) throws Exception {
		if (token == null){
			return false;
		}
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add("token", token);
		JSONObject resource = getResource(concat(this.uri, "valid"),
				queryParams);
		try {
			return resource.getBoolean("content");
		} catch (Exception e) {
			throw new Exception(concat(e.toString(), " ",
					resource.getString("content")));
		}
	}
	
	public boolean authorize(String type, String name, String action)  throws Exception{
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		
		queryParams.add("token", token);
		queryParams.add("type", type);
		queryParams.add("name", name);
		queryParams.add("action", action);
		
		JSONObject resource = getResource(concat(this.uri, "authorize"),
				queryParams);
		try {
			return resource.getBoolean("content");
		} catch (Exception e) {
			throw new Exception(concat(e.toString(), " ",
					resource.getString("content")));
		}
		//type=CGAPP.CG_SCREENS&schema=CARTONE&name=CGERP.INDEX&action=GET
	}

	/*
	 * public String senddata(String command, String data) { String result = "";
	 * try { String datastr = (data !=null) ? URLEncoder.encode(data, "UTF-8") :
	 * ""; String tokenstr = (this.token != null) ?
	 * URLEncoder.encode(this.token, "UTF-8"):"";
	 * 
	 * URL url = new URL(this.uri); URLConnection connection =
	 * url.openConnection(); connection.setDoOutput(true);
	 * 
	 * OutputStreamWriter out = new OutputStreamWriter(connection
	 * .getOutputStream()); out.write("data=" + datastr); out.write("&token=" +
	 * tokenstr); out.write("&procedure=" + command); out.close();
	 * 
	 * BufferedReader in = new BufferedReader(new InputStreamReader(
	 * connection.getInputStream()));
	 * 
	 * String decodedString;
	 * 
	 * while ((decodedString = in.readLine()) != null) { result +=
	 * decodedString; } in.close(); } catch (IOException e) {
	 * e.printStackTrace(); } return result; }
	 */

	public JSONObject senddata(String command, String data) throws Exception {
		return senddata(command, this.schema, data);
	}

	public JSONObject senddata(String command, String schema, String data)
			throws Exception {
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add("procedure", command);
		queryParams.add("schema", schema);
		queryParams.add("data", data);
		queryParams.add("token", this.token);

		return getResource(concat(this.uri, "server"), queryParams);
	}

	public Object getJsonData(String command, String data) throws Exception {
		return getJsonData(command, this.schema, data);
	}

	public Object getJsonData(String command, String schema, String data)
			throws Exception {
		JSONObject result = senddata(command, schema, data);

		if (result.getInt("status") == 200) {
			return result.get("content");
		} else if (result.getInt("status") == 401) {
			throw new UnauthorizedException(result.toString());
		} else if (result.getInt("status") == 403) {
			throw new UnauthorizedException(result.getString("content"));
		} else {
			throw new Exception(result.toString());
		}
	}

	public String getScreen(String screenname) throws Exception {
		cgProcedure name = new cgProcedure(screenname, this.schema);
		JSONObject screenDef = new JSONObject();
		if (name.getSchema().isEmpty()){
			screenDef.put("schema", this.schema);
		} else {
			screenDef.put("schema", name.getSchema());	
		}
			
		
		screenDef.put("table", "CGAPP.CGAP_SCREENS");
		screenDef.put("id", name.getName());

		JSONObject screen = (JSONObject) getJsonData("CLEARG.CG_RECORD", screenDef.toString());
		JSONArray data = screen.getJSONArray("data");
		if (data.length() == 0){
			throw new ResourceNotFoundException("Page Not Found");
		}
		return data.getJSONObject(0).getString("CODE");
	}

	public Object toJSON(String data) throws JSONException {
		Object json;
		if (data.startsWith("{")) {
			json = new JSONObject(data);
		} else {
			json = new JSONArray(data);
		}
		return json;
	}

	public static String concat(String... strs) {
		StringBuilder strbuild = new StringBuilder(strs[0]);
		for (int i = 1; i < strs.length; i++) {
			strbuild.append(strs[i]);
		}
		return strbuild.toString();
	}

	public String getCookieValue(Cookie[] cookies, String cookieName) {
		if (cookies == null) return null;
		for (int i = 0; i < cookies.length; i++) {
			Cookie cookie = cookies[i];
			if (cookieName.equals(cookie.getName()))
				return (cookie.getValue());
		}
		return null;
	}

	public static String getGlobalVar(String name) {
		try {
		return (String)initialContext.lookup("java:global/" + name);
		} catch(Exception e){
			return null;
		}
	}

	public static String getSchema(String url){
		StringBuilder strbuild = new StringBuilder("(.*).");
		strbuild.append(getGlobalVar("cgApp-baseUrl"));

		Pattern pattern;
		pattern = Pattern.compile(strbuild.toString());
		
		String schema;

		Matcher m = pattern.matcher(url);
		if (m.find()) {
			schema = m.group(1);
		} else {
			schema = "NONE";
		}
		return schema;
	}
	
	public static String encodeURIComponent(String component)   {     
		String result = null;      
		
		try {       
			result = URLEncoder.encode(component, "UTF-8")   
				   .replaceAll("\\%28", "(")                          
				   .replaceAll("\\%29", ")")   		
				   .replaceAll("\\+", "%20")                          
				   .replaceAll("\\%27", "'")   			   
				   .replaceAll("\\%21", "!")
				   .replaceAll("\\%7E", "~");     
		} catch (UnsupportedEncodingException e) {       
			result = component;     
		}      
		
		return result;   
	}   
}