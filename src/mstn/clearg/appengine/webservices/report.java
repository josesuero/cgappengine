package mstn.clearg.appengine.webservices;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.jersey.core.util.MultivaluedMapImpl;

import mstn.clearg.appengine.CGAppEngine;

@Path("/report")
public class report {

	@Context
	HttpServletRequest request;

	@GET
	public Response get(@QueryParam("template") String template,
			@QueryParam("data") String data, @QueryParam("format") String format) {
		try {
			HttpSession session = request.getSession();
			String schema = CGAppEngine.getSchema(request.getServerName());
			if (schema == null) {
				schema = "none";
			}
			String token = (String) session.getAttribute("token");
			CGAppEngine cgAppEngine = new CGAppEngine(schema, token);

			JSONObject templateData = new JSONObject();
			templateData.put("table", "CGAPP.CGAP_REPORTS");
			templateData.put("fields", "CGAPP.CGAP_REPORTS");
			templateData.put("filter", new JSONArray().put(new JSONObject().put("NAME", template)));

			//JSONObject templateBody = cgAppEngine.senddata("CLEARG.CG_GET",	templateData.toString());
			JSONObject templateBody = new JSONObject(template);
			
			MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
			queryParams.add("template", templateBody.toString());
			queryParams.add("data", data);
			queryParams.add("format", format);
			
			String response = cgAppEngine.postResourceData(CGAppEngine.getGlobalVar("cgApp-ReportUrl"), queryParams);
			
			return Response.ok(response).build();
		} catch (Exception e) {
			StringBuilder errorMessage = new StringBuilder(e.getMessage().toString());
			for (int i = 0; i < e.getStackTrace().length;i++){
				errorMessage.append(" \n");
				errorMessage.append(e.getStackTrace()[i].toString());
			}
			return Response.serverError().entity(errorMessage.toString()).status(500)
					.build();
		}
	}
	
	@POST
	@Produces("application/pdf")
	public Response post(@FormParam("template") String template,
			@FormParam("data") String data, @FormParam("format") String format) {
		return get(template, data, format);
	}
}
