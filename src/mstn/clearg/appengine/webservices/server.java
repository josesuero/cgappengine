package mstn.clearg.appengine.webservices;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import mstn.clearg.appengine.CGAppEngine;
import mstn.clearg.cache.CGCache;
import mstn.clearg.cache.CGCache.CacheLevel;

@Path("/server")
public class server {
	
	@Context HttpServletRequest request;
	private CGCache cacheGlobal;
	
	public server(){
		cacheGlobal = new CGCache("", "", CacheLevel.Global);
	}
	
	@PostConstruct
	public void init(){
		
	}
	
	public JSONObject process(String procedure, String data) throws Exception{
		HttpSession session = request.getSession();
		String schema = CGAppEngine.getSchema(request.getServerName());
		if (schema == null){
			schema = "none";
		}
		String token = (String) session.getAttribute("token");
		if (token == null){
			token = (String) session.getAttribute("innerToken");
		}
		CGAppEngine cgAppEngine = new CGAppEngine(schema, token);
		
		return cgAppEngine.senddata(procedure.toUpperCase(), data);
		
	}
	
	@GET
	public String getRegular(@QueryParam("procedure") String procedure, @QueryParam("data") String data) throws Exception{
		return process(procedure, data).toString();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String postRegular(@FormParam("procedure") String procedure, @FormParam("data") String data) throws Exception{
		return process(procedure, data).toString();
	}

	@GET
	@Path("/cache/{type:.+}/{procedure:.*}.cg")
	public Response getcachejson(@PathParam("procedure") String procedure, @QueryParam("data") String data, @PathParam("type") String type, @QueryParam("cached") String cache) throws Exception{
		String content = null;
		boolean cached;
		String status = "200";
		String cacheName = CGAppEngine.concat("GLOBAL_PROCEDURES_", procedure,data);
		if (cache != null && cache.equalsIgnoreCase("false")){
			cached = false;
		} else {
			cached = true;
		}
		
		if (cacheGlobal != null && cached == true) {
			content = (String) cacheGlobal.get(cacheName);
		}
		if (content == null){
			JSONObject procedureResult = process(procedure, data); 
			content = procedureResult.getString("content");
			status = procedureResult.getString("status");
		}
		if (cached == true && status.equals("200")){
			cacheGlobal.put(cacheName, content);	
		} else {
			if (cacheGlobal.exists(cacheName)){
				cacheGlobal.remove(cacheName);
			}
		}
    	String mediaType = "aplication/json";
    	if (type.equalsIgnoreCase("json")){
    		mediaType = MediaType.APPLICATION_JSON;
    	} else if (type.equalsIgnoreCase("html")) {
    		mediaType = MediaType.TEXT_HTML;
    	} else if (type.equalsIgnoreCase("text")){
    		mediaType = MediaType.TEXT_PLAIN;
    	} else if (type.equalsIgnoreCase("xml")){
    		mediaType = MediaType.APPLICATION_XML;
    	} else if (type.equalsIgnoreCase("js")){
    		mediaType = "text/javascript";
    	}
    	return Response.ok(content).type(mediaType).build();
		
	}
	
	@GET
	@Path("/{type:.+}/{procedure:.*}.cg")
	public Response getjson(@PathParam("procedure") String procedure, @QueryParam("data") String data, @PathParam("type") String type) throws Exception{
		
    	String content = process(procedure, data).getString("content");
    	String mediaType = "aplication/json";
    	if (type.equalsIgnoreCase("json")){
    		mediaType = MediaType.APPLICATION_JSON;
    	} else if (type.equalsIgnoreCase("html")) {
    		mediaType = MediaType.TEXT_HTML;
    	} else if (type.equalsIgnoreCase("text")){
    		mediaType = MediaType.TEXT_PLAIN;
    	} else if (type.equalsIgnoreCase("xml")){
    		mediaType = MediaType.APPLICATION_XML;
    	}
    	return Response.ok(content).type(mediaType).build();

	}

	
	@GET
	@Path("/{procedure:.*}.cg")
	public String get(@PathParam("procedure") String procedure, @QueryParam("data") String data) throws Exception{
		return process(procedure, data).toString();
	}
	
	
	@POST
	@Path("/{type:.+}/{procedure:.*}.cg")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response postType(@PathParam("procedure") String procedure, @FormParam("data") String data, @PathParam("type") String type) throws Exception{
		return getjson(procedure, data, type);
	}
	
	@POST
	@Path("/{procedure:.*}.cg")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String post(@PathParam("procedure") String procedure, @FormParam("data") String data) throws Exception{
		return process(procedure, data).toString();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String postjson(@FormParam("procedure") String procedure, @FormParam("data") String data) throws Exception{
		return process(procedure, data).getString("content");
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public String posttext(@FormParam("procedure") String procedure, @FormParam("data") String data) throws Exception{
		return process(procedure, data).getString("content");
	}

}
