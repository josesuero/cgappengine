package mstn.clearg.appengine.webservices;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;

import org.json.JSONObject;

import mstn.clearg.appengine.CGAppEngine;

@Path("/{type:login|logout}")
public class sso {
	CGAppEngine cgAppEngine = new CGAppEngine("schema", "");

	@Context
	HttpServletRequest request;

	public String login(String username, String password, String schema)
			throws Exception {

		String token = cgAppEngine.login(username, password, schema);

		return token;
	}

	public String logout() throws Exception {
		request.logout();
		//KeycloakPrincipal principal =  (KeycloakPrincipal)request.getUserPrincipal();
		//principal.getKeycloakSecurityContext().getToken().getRealmAccess();
		return "";
		//HttpSession session = request.getSession();
		//String token = (String) session.getAttribute("token");
		//session.removeAttribute("token");
		//return cgAppEngine.logout(token);
	}

	@GET
	public String get(@PathParam("type") String type,
			@QueryParam("username") String user,
			@QueryParam("password") String password) {
		String schema = CGAppEngine.getSchema(request.getServerName());
		;
		try {

			if (type.equalsIgnoreCase("login")) {
				String token = login(user, password, schema);
				HttpSession session = request.getSession();
				if (!token.isEmpty()) {
					session.setAttribute("token", token);
					return new JSONObject().put("result", true).toString();
				} else {
					return new JSONObject().put("result", false).toString();
				}
			} else if (type.equalsIgnoreCase("logout")) {
				return logout();
			}
		} catch (Exception e) {
			return "error:" + e.toString();
		}
		return "not implemented";
	}

	@POST
	public String post(@PathParam("type") String type,
			@FormParam("username") String user,
			@FormParam("password") String password) {
		return get(type, user, password);
	}
}
