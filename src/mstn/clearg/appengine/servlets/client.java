package mstn.clearg.appengine.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mstn.clearg.appengine.UnauthorizedException;
import mstn.clearg.appengine.CGAppEngine;
import mstn.clearg.cache.CGCache;
import mstn.clearg.cache.CGCache.CacheLevel;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.resource.loader.StringResourceLoader;
import org.apache.velocity.runtime.resource.util.StringResourceRepository;
import org.apache.velocity.tools.ToolManager;
import org.json.JSONObject;

/**
 * Servlet implementation class client
 */
@WebServlet({ "/client", "*.cgsc" })
public class client extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CGAppEngine cgAppEngine;
	private ToolManager toolmanager;
	private CGCache cacheGlobal;
	private boolean securityEnabled = false;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	VelocityEngine ve;
	public client() {
		super();
		toolmanager = new ToolManager(true);
		cacheGlobal = new CGCache("", "", CacheLevel.Global);
		ve = new VelocityEngine();
		ve.setProperty("resource.loader", "string");
		ve.setProperty("string.resource.loader.class",
				"org.apache.velocity.runtime.resource.loader.StringResourceLoader");
		ve.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");

		try {
			ve.init();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try {
			long starttime = System.currentTimeMillis();
			HttpSession session = request.getSession();

			/*
			 * Eliminated / Security Risk if (session.isNew()) { String encUrl =
			 * response.encodeURL(request.getRequestURL() .toString());
			 * response.sendRedirect(encUrl); }
			 */

			if (request.getParameter("mime") != null) {
				response.setContentType(request.getParameter("mime"));
			}

			// loginUri = cgAppEngine.getGlobalVar("cgApp-LoginUrl");

			// if (loginUri.indexOf("http") == -1){
			// loginUri = "http://localhost:" + request.getLocalPort() +
			// loginUri;
			// }

			// String token = request.getParameter("token");

			String screenname = request.getParameter("screen");
			String schema = CGAppEngine.getSchema(request.getServerName());
			String token = (String) session.getAttribute("token");

			if (schema == null) {

			}
			boolean cached;
			if (request.getParameter("cached") == null) {
				if (screenname == null) {
					cached = true;
				} else {
					cached = false;
				}
			} else if (request.getParameter("cached").equalsIgnoreCase("false")) {
				cached = false;
			} else if (request.getParameter("cached").equalsIgnoreCase("true")) {
				cached = true;
			} else {
				cached = true;
			}

			if (screenname == null) {
				screenname = "CGERP.INDEX";
			}
			
			if (screenname.toUpperCase().contains("PUBLIC_")){
				token = (String) session.getAttribute("innerToken");
			}

			String screenReturn = null;

			this.cgAppEngine = new CGAppEngine(schema, token);
			if (securityEnabled && (token == null || !cgAppEngine.isValid(token))) {
				// response.sendRedirect(cgAppEngine.concat("http://",schema,".sso.itineris.do/sso/UI/Login?goto=",
				// URLEncoder.encode(request.getRequestURL().toString(),
				// "UTF-8")));
//				response.sendRedirect(CGAppEngine.concat("/sso/UI/Login?goto=",
//						URLEncoder.encode(request.getRequestURL().toString(),
//								"UTF-8")));
				if (token != null)
					session.removeAttribute("token");
				StringBuilder queryString = new StringBuilder();
				Enumeration<String> paramNames = request.getParameterNames();
				while(paramNames.hasMoreElements())
				{
				      String paramName = paramNames.nextElement();
				      if (queryString.length() != 0)
				    	  queryString.append("&");
				      queryString.append(paramName).append("=").append(request.getParameter(paramName));
				}
				StringBuilder loginurl = new StringBuilder();
				loginurl.append(request.getRequestURL().toString()).append("?").append(queryString);
				
				
				response.sendRedirect(CGAppEngine.concat(CGAppEngine.getGlobalVar("cgApp-loginURL"),"?goto=",CGAppEngine.encodeURIComponent(loginurl.toString())));
				
				return;
			}

			this.cgAppEngine.setToken(token);

			String parsedScreen = cgAppEngine.getScreen(screenname);

			String cacheName = "";
			if (cacheGlobal != null && cached == true) {
				cached = true;
				cacheName = CGAppEngine.concat("GLOBAL_SCREENS_", screenname);
				screenReturn = (String) cacheGlobal.get(cacheName);
			}

			if (request.getParameter("recache") != null && request.getParameter("recache").equalsIgnoreCase("true")) {
				cached = true;
				screenReturn = null;
			}

			if (screenReturn == null) {
				// VelocityContext context = new VelocityContext();
				Properties props;
				VelocityContext context;
				/*
				props = new Properties();
				props.put("resource.loader", "string");
				props.put("string.resource.loader.class",
						"org.apache.velocity.runtime.resource.loader.StringResourceLoader");
				*/
				// VelocityContext context = new VelocityContext();

				context = new VelocityContext(this.toolmanager.createContext());

				
				//ve.init(props);

				// String screenstr = (String)
				// cgAppEngine.senddata("CGAPP.SCREENS_GETCODE", screenname);

				// String parsedScreen = java.net.URLDecoder.decode(screenstr,
				// "UTF-8");

				// JSONObject header = (JSONObject) screen.get("header");

				context.put("token", token);
				context.put("schema", schema);
				context.put("cgApp", cgAppEngine);

				String data = request.getParameter("data");
				JSONObject dataobj = null;
				if (!(data == null) && !(data.equals(""))) {
					if (data.startsWith("{")) {
						dataobj = new JSONObject(data);
						context.put("data", dataobj);
					} else {
						context.put("data", data);
					}
				} else {
					context.put("data", "");
				}

				context.put("currentuser", request.getUserPrincipal());
				context.put("screenname", screenname.replace(".", "_"));

				Template template = null;

				StringResourceRepository repo = StringResourceLoader
						.getRepository();

				repo.putStringResource(screenname, parsedScreen);

				StringWriter sw = new StringWriter();
				
				
				ve.getTemplate(screenname).merge(context, sw);

				parsedScreen = sw.toString();
				JSONObject screen = new JSONObject();

				boolean parse = request.getParameter("parse") == null
						|| !request.getParameter("parse").equalsIgnoreCase(
								"false");
				boolean process = (request.getParameter("process") == null || !request
						.getParameter("process").equalsIgnoreCase("false"))
						&& (parse);

				if (parse) {
					screen = new JSONObject(parsedScreen);
				}
				if (process) {
					// process template
					if (screen.getJSONObject("header").has("name")) {
						screenname = screen.getJSONObject("header")
								.getString("name").replace(".", "_");
						context.put("screenname", screenname);
					}

					context.put("page", screen);
					context.put("controls", screen.getJSONArray("controls"));

					// cgAppEngine.senddata(command, data)

					// Load Templates to Engine
					String myTemplateName = "";
					if (dataobj != null && dataobj.has("template")) {
						myTemplateName = dataobj.getString("template");
					} else {
						myTemplateName = screen.getJSONObject("header")
								.getString("template");
					}

					// String myTemplate = java.net.URLDecoder.decode((String)
					// cgAppEngine.senddata("CGAPP.SCREENS_GETCODE",
					// myTemplateName),"UTF-8");
					String myTemplate = cgAppEngine.getScreen(myTemplateName);
					
					//TODO fix dollar escape better
					myTemplate = myTemplate.replace("$.", "${esc.d}.");

					if (myTemplate.trim().startsWith("cg:imports")) {
						int end = myTemplate.indexOf(';');
						String[] includes = myTemplate.trim()
								.substring(10, end).trim().split(",");
						for (String include : includes) {
							// String templatestring = (String)
							// cgAppEngine.senddata("CGAPP.SCREENS_GETCODE",
							// include.trim());
							String templatestring = cgAppEngine
									.getScreen(include.trim());
							String includename;
							if (include.trim().contains(".")) {
								includename = include.trim().split("\\.")[1];
							} else {
								includename = include.trim();
							}
							repo.putStringResource(includename, templatestring);
						}
						myTemplate = myTemplate.substring(end + 1);
					}

					ArrayList<String> includelist = new ArrayList<String>();
					for (Object obj : screen.getJSONArray("controls")) {
						JSONObject var = new JSONObject(obj.toString());
						String includename;
						String objschema = "CGAPP";
						if (var.getString("type").contains(".")) {
							objschema = var.getString("type").split("\\.")[0];
							includename = var.getString("type").split("\\.")[1];
						} else {
							includename = var.getString("type").trim();
						}
						if (!includelist
								.contains(objschema + "." + includename)) {
							// String templatestring = (String)
							// cgAppEngine.senddata("CGAPP.SCREENS_GETCODE",
							// objschema + "."+ includename, uri);
							String templatestring = (String) cgAppEngine
									.getScreen(objschema + "." + includename);
							// repo.putStringResource(includename,
							// java.net.URLDecoder.decode(templatestring,
							// "UTF-8"));
							repo.putStringResource(includename, templatestring);
							includelist.add(objschema + "." + includename);
						}
					}

					repo.putStringResource(myTemplateName, myTemplate);

					template = ve.getTemplate(myTemplateName);

					sw = new StringWriter();
					template.merge(context, sw);

					//ve = null;
					repo = null;
					template = null;

					screenReturn = sw.toString();
				} else {
					if (parse) {
						screenReturn = screen.toString();
					} else {
						screenReturn = sw.toString();
					}
				}
				if (cacheGlobal != null && cached) {
					cacheGlobal.put(cacheName, screenReturn);
				}
			} else {
				response.setHeader("ClearG-cached", "true");
			}
			out.print(screenReturn);

			long endttime = System.currentTimeMillis();

			if (request.getParameter("gentime") != null) {
				out.print(endttime - starttime);
			}
		} catch (ResourceNotFoundException ex) {
			// couldn't find the template
			out.print(ex.getMessage());
		} catch (ParseErrorException ex) {
			// syntax error: problem parsing the template
			out.print(ex.getMessage());
		} catch (MethodInvocationException ex) {
			// something invoked in the template
			out.print(ex.getMessage());
		} catch (UnauthorizedException ex) {
			out.print(ex.getMessage());
		} catch (Exception ex) {
			out.print(ex.toString());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
