package mstn.clearg.appengine.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class regex
 */
@WebServlet("/regex")
public class regex extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public regex() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();

		String result = "";

		String str = "cartone.cg.mstn.com";
		String pattern = "(.*).cg.mstn.com";
		String type = "regex";

		pattern = "ordena";
		type = "value";

		if (type == "regex") {
			Pattern regex = Pattern.compile(pattern);
			Matcher m = regex.matcher(str);
			if (m.find()) {
				result = m.group(1);
			} else {
				result = "not found";
			}
		} else {
			result = pattern;
		}
		out.print(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
