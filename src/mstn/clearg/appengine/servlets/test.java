package mstn.clearg.appengine.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class test
 */
@WebServlet("/test")
public class test extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public test() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		 InitialContext ic;
		try {
			ic = new InitialContext();
			String javahowto = (String)ic.lookup("java:global/cgApp-Url");
			out.println(javahowto);
		} catch (NamingException e) {
			e.printStackTrace(out);
		}
		 
		
		HttpSession session = request.getSession();
		out.println("name " + session.getAttribute("name"));
		
		response.addCookie(new Cookie("schema", "mstnmedia"));
		
		if (session.isNew()){
			String url = request.getRequestURL().toString();
			String encUrl = response.encodeURL(url);
			session.setAttribute("name", "testCookie");			
			out.print(session.getId());
			
			response.sendRedirect(encUrl);
		}
		Cookie[] cookies = request.getCookies();
		if (cookies != null)
			out.println("cookies length " + request.getCookies().length);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
