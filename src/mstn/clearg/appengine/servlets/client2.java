package mstn.clearg.appengine.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.jersey.api.client.Client;

//import com.sun.jersey.core.util.MultivaluedMapImpl;

/**
 * Servlet implementation class client2
 */
@WebServlet("/client2")
public class client2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Client clientRequest;
	//private String internalToken;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public client2() {
        super();
    }
    
    public void init(ServletConfig config) throws ServletException{
    	super.init(config);
    	clientRequest = Client.create();
			try {
				//internalToken = login();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out =  response.getWriter();
		try {
			clientRequest.toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace(out);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
