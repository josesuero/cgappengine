package mstn.clearg.appengine;

public class UnauthorizedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 418468480916033944L;
	
	public UnauthorizedException(String message){
		super(message);
	}
}
