package mstn.clearg.appengine.filters;

import java.io.File;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import mstn.clearg.appengine.CGAppEngine;
import mstn.clearg.language.cgProcedure;

public class UrlRewriteFilter implements Filter {

	private static String innerToken;

	@Override
	public void init(FilterConfig config) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws ServletException, IOException {
		try {
			HttpServletRequest request = (HttpServletRequest) req;
			String requestURI = request.getRequestURI();

			File file = new File(requestURI);

			String[] path = file.getParent().split("/");

			String extension = file.getName().substring(
					file.getName().lastIndexOf('.'));

			String schema = CGAppEngine.getSchema(request.getServerName());

			cgProcedure fileName = new cgProcedure(file.getName().replace(extension, ""), schema);
			String prefix = "";
			StringBuilder extraPath = new StringBuilder();
			for (String part : path) {
				if (part.equalsIgnoreCase("public")) {
					CGAppEngine cgAppEngine = new CGAppEngine("", "");
					if (innerToken == null) {
						innerToken = cgAppEngine.login("anonymous","anonymous", "");
					} else if (!cgAppEngine.isValid(innerToken)){
						innerToken = cgAppEngine.login("anonymous","anonymous", "", innerToken);
					}
					request.getSession().setAttribute("innerToken", innerToken);
					prefix = "public_";
				} else if (part.equalsIgnoreCase("cgappengine")){
				} else if (!part.equals("")){
					extraPath.append("/").append(part);
				}

				if (part.equalsIgnoreCase("mobile")) {
					// move to mobile
				}
			}

			StringBuilder newURI = new StringBuilder("");
			if (extension.equals(".cgs")) {
				newURI.append("/client");
				newURI.append("?").append("screen=")
						.append(fileName.getSchema()).append(".")
						.append(prefix).append(fileName.getName())
						.append("&");

			} else if (extension.equals(".cgp")) {
				newURI.append("/clearg/server");
				newURI.append(extraPath.toString());
				newURI.append("/")
				.append(fileName.getSchema()).append(".")
				.append(prefix).append(fileName.getName())
				.append(".cg")
				.append("?");
				
			}
			
			if (request.getQueryString() != null) {
				newURI.append(request.getQueryString());
			}
			req.getRequestDispatcher(newURI.toString()).forward(req, res);

			// chain.doFilter(req, res);

		} catch (Exception e) {
			e.printStackTrace(res.getWriter());
		}
	}

	@Override
	public void destroy() {
		//
	}
}