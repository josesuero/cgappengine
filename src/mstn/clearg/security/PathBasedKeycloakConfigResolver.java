package mstn.clearg.security;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.json.Json;
import javax.json.JsonObject;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.KeycloakDeploymentBuilder;
import org.keycloak.adapters.OIDCHttpFacade;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.ClientsResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.RealmRepresentation;

public class PathBasedKeycloakConfigResolver implements KeycloakConfigResolver {

	private final Map<String, KeycloakDeployment> cache = new ConcurrentHashMap<String, KeycloakDeployment>();
	private static String baseUrl = "";
	//private static final Logger log = Logger.getLogger( PathBasedKeycloakConfigResolver.class.getName() );
	private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PathBasedKeycloakConfigResolver.class);
	
	
	private static String getGlobalVar(String name) throws NamingException {
		return (new InitialContext()).lookup(name).toString();
	}

	private static InputStream genJSON(String schema) throws NamingException {
		String authServer = getGlobalVar("java:global/ssouri");
		// Keycloak keycloak = Keycloak.getInstance(authServer, "example",
		// "examples-admin-client", "password", "examples-admin-client",
		// "password");
		String realmUser = getGlobalVar("java:global/realmuser");
		String realmPass = getGlobalVar("java:global/realmpass");
		
		log.info( "Schema: " + schema );
		log.info( "Server: " + authServer + " realmUser: " + realmUser + " RealmPass: "+realmPass);
		
		Keycloak keycloak = Keycloak.getInstance(authServer, "master", realmUser,
				realmPass, realmUser, realmPass);
		RealmResource realm = keycloak.realm(schema);
		RealmRepresentation realmInfo = realm.toRepresentation();

		ClientsResource clients = realm.clients();
		for (ClientRepresentation client : clients.findAll()) {
			if (!client.getClientId().equals("itineris"))
				continue;
			JsonObject json = Json.createObjectBuilder().add("realm", realmInfo.getId())
					.add("realm-public-key", realmInfo.getPublicKey()).add("auth-server-url", authServer)
					.add("ssl-required", realmInfo.getSslRequired()).add("resource", client.getClientId())
					.add("public-client", client.isPublicClient()).build();
			
			log.info("Realm: " + schema + " json: " + json.toString());

			InputStream stream = new ByteArrayInputStream(json.toString().getBytes(StandardCharsets.UTF_8));
			return stream;
		}
		throw new NamingException("Schema not found");
	}

	public static String getSchema(String url) throws NamingException {
		if (baseUrl.isEmpty()) {
			baseUrl = getGlobalVar("java:global/cgApp-baseUrl");
		}

		StringBuilder strbuild = new StringBuilder("(.*).");
		strbuild.append(baseUrl);

		Pattern pattern;
		pattern = Pattern.compile(strbuild.toString());

		String schema;

		Matcher m = pattern.matcher(url);
		if (m.find()) {
			schema = m.group(1);
		} else {
			schema = "NONE";
		}
		return schema;
	}

	@Override
	public KeycloakDeployment resolve(OIDCHttpFacade.Request request) {
		String path = request.getURI();
		path = path.substring(path.indexOf("//") + 2);
		String realm;
		try {
			realm = getSchema(path);
		} catch (NamingException e) {
			throw new IllegalStateException("Not able to resolve realm from the request path!");
		}
		if (realm.contains("?")) {
			realm = realm.split("\\?")[0];
		}

		KeycloakDeployment deployment = cache.get(realm);
		if (null == deployment) {
			// not found on the simple cache, try to load it from the file
			// system
			InputStream is;
			try {
				is = genJSON(realm);
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				throw new IllegalStateException("Not able to find the file /" + realm + "-keycloak.json");
			}

			deployment = KeycloakDeploymentBuilder.build(is);
			cache.put(realm, deployment);
		}

		return deployment;
	}

}